package ihm;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class main {

	public static void main(String[] args) {
	
		Frame f = new Frame("Cda Personne");
		f.setFocusable(true);
		f.addWindowListener(new FrameListener());
		f.addKeyListener(new ClavierListener());
		GridLayout layout = new GridLayout(4, 2);
		f.setLayout(layout);
		Label labelNom = new Label("Nom : ");
		labelNom.addKeyListener(new ClavierListener());
		TextField saisieNom = new TextField();
		saisieNom.addKeyListener(new ClavierListener());
		Label labelPrenom = new Label("Prenom : ");
		TextField saisiePrenom = new TextField();
		
		Label labelAge = new Label("Age : ");
		TextField saisieAge = new TextField();
		Button b = new Button("Afficher");
		
		f.add(labelNom);
		f.add(saisieNom);
		
		f.add(labelPrenom);
		f.add(saisiePrenom);
		
		
		f.add(labelAge);
		f.add(saisieAge);
		
		b.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(saisieNom.getText());
				System.out.println(saisiePrenom.getText());
				System.out.println(saisieAge.getText());
				
			}
		});
		f.add("bouton", b);
		
		f.pack();
		f.setFocusable(true);
		f.setSize(400, 400);
		f.pack();
		f.setVisible(true);

	}

}
