package fr.afpa.entite;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Conseiller extends Personne implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HashMap<String, Client> portefeuilleClient;
	
	private static int nbConseillier;
	
	
	public Conseiller() {
		super();
	}

	public Conseiller(String nom, String prenom, LocalDate dateNaissance, String mail) {
		super(nom, prenom, dateNaissance, mail);
		this.setLogin("CO" + String.format("%04d", ++nbConseillier));
		portefeuilleClient=new HashMap<String, Client>();
	}

	public Map<String, Client> getPortefeuilleClient() {
		return portefeuilleClient;
	}

	public void setPortefeuilleClient(Map<String, Client> portefeuilleClient) {
		this.portefeuilleClient = (HashMap<String, Client>) portefeuilleClient;
	}
	
	

	public static int getNbConseillier() {
		return nbConseillier;
	}

	public static void setNbConseillier(int nbConseillier) {
		Conseiller.nbConseillier = nbConseillier;
	}

	@Override
	public String toString() {
		return "\nConseiller ["+super.toString()+"portefeuilleClient=" + portefeuilleClient + "]";
	}

	@Override
	public int hashCode() {
		return 	Integer.parseInt(this.getLogin().substring(2));
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) && portefeuilleClient.equals(((Conseiller)obj).getPortefeuilleClient());
	}	
}
