package fr.afpa.calculatriceGraphique.ihms;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fr.afpa.calculatriceGraphique.metiers.ActionListenerCalculatrice;

public class MenuCalculatrice extends JMenuBar{

	private JMenu menu;
	private JMenuItem menuItem;
	private ActionListenerCalculatrice actionListenerBoutons;

	public MenuCalculatrice(ActionListenerCalculatrice actionListenerBoutons) {
		super();
		
		this.actionListenerBoutons=actionListenerBoutons;
		initialisation();
		
	}
	
	public void initialisation() {
		menu = new JMenu("Fichier");
		
		menuItem = new JMenuItem("Reset");
		menuItem.setActionCommand("C");
		menuItem.addActionListener(actionListenerBoutons);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Histo");
		menuItem.setActionCommand("histo");
		menuItem.addActionListener(actionListenerBoutons);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Effacer historique");
		menuItem.setActionCommand("effacer");
		menuItem.addActionListener(actionListenerBoutons);
		menu.add(menuItem);
		
		menuItem = new JMenuItem("Quitter");
		menuItem.setActionCommand("quitter");
		menuItem.addActionListener(actionListenerBoutons);
		menu.add(menuItem);
		add(menu);
		menu = new JMenu("?");
		
		menu.setActionCommand("?");
		menuItem = new JMenuItem("aide");
		menuItem.setActionCommand("aide");
		menuItem.addActionListener(actionListenerBoutons);
		menu.add(menuItem);
		
		add(menu);
	}

	
}
