package fr.afpa.calculatriceGraphique.ihms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.afpa.calculatriceGraphique.metiers.ActionListenerCalculatrice;

public class Calculatrice extends JFrame implements Runnable {

	private JButton bouton;
	private JTextField jTextFieldEcran;
	private JTextField jTextFieldCalcul;
	
	private JPanel jPanelEcran;
	private JPanel jPanelEcran2;
	private JPanel jPanelBoutons;
	private JPanel jPanelBoutonsZero;
	private JPanel jPanelBoutonsOperateur;
	
	
	private ActionListenerCalculatrice actionListenerBoutons;
	private MenuCalculatrice menuCalculatrice;

	public Calculatrice(String title) throws HeadlessException {
		super(title);
		setBounds(600, 400, 225, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		actionListenerBoutons = new ActionListenerCalculatrice();
		setResizable(false);
		
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		initialisation();

	}

	public void initialisation() {
		GridLayout gridLayout;
		menuCalculatrice = new MenuCalculatrice(actionListenerBoutons);
		setJMenuBar(menuCalculatrice);
		
		jPanelEcran= new JPanel();
		jPanelEcran2= new JPanel();
		gridLayout = new GridLayout(1, 1);
		gridLayout.setHgap(0);
		gridLayout.setVgap(0);
		jPanelEcran.setLayout(gridLayout);
		jPanelEcran2.setLayout(new GridLayout(1, 2));
		
		jTextFieldCalcul = new JTextField("");
		jTextFieldCalcul.setEditable(false);
		jPanelEcran2.add(jTextFieldCalcul);
		
		bouton = new JButton("C");
		bouton.setActionCommand("C");
		bouton.addActionListener(actionListenerBoutons);
		jPanelEcran2.add(bouton);
		
		//jPanelEcran.add(jPanelEcran2);

		jTextFieldEcran = new JTextField("");
		jTextFieldEcran.setEditable(false);
		jPanelEcran.add(jTextFieldEcran);
		getContentPane().add(jPanelEcran, BorderLayout.NORTH);

		jPanelBoutons = new JPanel();
		 gridLayout = new GridLayout(3, 4);
		gridLayout.setHgap(2);
		gridLayout.setVgap(2);
		jPanelBoutons.setLayout(gridLayout);
		String[] boutons = { "7", "8", "9", "4", "5", "6", "1", "2", "3" };
		for (int i = 0; i < boutons.length; i++) {
			bouton = new JButton("" + boutons[i]);
			bouton.addActionListener(actionListenerBoutons);
			jPanelBoutons.add(bouton);

		}
		getContentPane().add(jPanelBoutons, BorderLayout.WEST);

		jPanelBoutonsZero = new JPanel();
		 gridLayout = new GridLayout(2, 1);
			gridLayout.setHgap(10);
			gridLayout.setVgap(40);
		jPanelBoutonsZero.setLayout(gridLayout);
		bouton = new JButton("0");
		bouton.addActionListener(actionListenerBoutons);
		jPanelBoutonsZero.add(bouton);
		
		bouton = new JButton("=");
		bouton.addActionListener(actionListenerBoutons);
		jPanelBoutonsZero.add(bouton);
		
		getContentPane().add(jPanelBoutonsZero, BorderLayout.CENTER);

		jPanelBoutonsOperateur = new JPanel();
		jPanelBoutonsOperateur.setLayout(new GridLayout(4, 1));
		String[] operateur = { "+", "-", "*", "/" };
		for (int i = 0; i < operateur.length; i++) {
			bouton = new JButton(operateur[i]);
			bouton.addActionListener(actionListenerBoutons);
			jPanelBoutonsOperateur.add(bouton);
		}
		getContentPane().add(jPanelBoutonsOperateur, BorderLayout.EAST);

	}

	public void calcul() {
		jTextFieldCalcul.setText(actionListenerBoutons.getCalcul());
		jTextFieldEcran.setBackground(Color.WHITE);
		if(!"".equals(actionListenerBoutons.getMessage())){
			jTextFieldEcran.setBackground(Color.RED);
			jTextFieldEcran.setText(actionListenerBoutons.getMessage());
		}else if (!"".equals(actionListenerBoutons.getResultat())) {
			jTextFieldEcran.setText(actionListenerBoutons.getResultat());
		} else if ("".equals(actionListenerBoutons.getNb2())) {
			jTextFieldEcran.setText(actionListenerBoutons.getNb1());
		} else {
			jTextFieldEcran.setText(actionListenerBoutons.getNb2());
		}
	}

	public void run() {
		while (true) {
			calcul();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
