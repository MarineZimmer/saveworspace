package fr.afpa.services;

public class ControleMailIdentifiant {
	/**
	 * Controle si le mail est vailde
	 * 
	 * @param mail mail a controler
	 * @return true si le mail est valide, false sinon
	 */
	public static boolean isMail(String mail) {
		return mail.matches("^\\w[\\w\\-]*(\\.[\\w\\-]+)*@[a-zA-Z]{3,13}\\.[a-zA-Z]{2,3}$");
	}
	
	/**
	 * controle si le login est un login Administrateur(ADM + 2 chiffres)
	 * @param login : la chaine de caracteres a controler
	 * @return : true si le login est un login Administrateur
	 */
	public static boolean isAdminitrateur(String login) {
		return login.matches("^ADM[0-9]{2}$");
	}

	/**
	 * controle si le login est un login Conseiller(CO + 4 chiffres)
	 * @param login : la chaine de caracteres a controler
	 * @return : true si le login est un login Conseiller
	 */
	public static boolean isConseiller(String login) {
		return login.matches("^CO[0-9]{4}$");
	}

	/**
	 * controle si le identifiant est un identifiant Client(2 Majuscules + 6 chiffres
	 * @param identifiant : la chaine de caracteres a controler
	 * @return : true si l'identifiant est un identifiant Client
	 */
	public static boolean isIdentifiantClient(String identifiant) {
		return identifiant.matches("^[A-Z]{2}[0-9]{6}$");
	}
	
	/**
	 * controle si le login est un login Client(10 chiffres)
	 * @param login : la chaine de caracteres a controler
	 * @return : true si le login est un login Client
	 */
	public static boolean isLoginClient(String login) {
		return login.matches("^[0-9]{10}$");
	}

}
