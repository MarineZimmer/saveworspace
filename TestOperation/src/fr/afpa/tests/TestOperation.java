package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.services.OperationService;
import junit.framework.TestCase;

public class TestOperation extends TestCase {
	int nb1;
	int nb2;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		nb1 = 10;
		nb2 = 5;
	}

	@Test
	public void testAddition() {
		int resultat = OperationService.addition(nb1, nb2);
		assertEquals("le resultat de l'addition est faux", nb1 + nb2, resultat);
	}
	
	@Test
	public void testSoustraction() {
		int resultat = OperationService.sousctraction(nb1, nb2);
		assertEquals("le resultat de la soustraction est faux", nb1 - nb2, resultat);
	}
	
	@Test
	public void testMultiplication() {
		int resultat = OperationService.multiplication(nb1, nb2);
		assertEquals("le resultat de la multiplication est faux", nb1 * nb2, resultat);
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}

}
