package fr.afpa.tests;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestsAppli extends TestCase {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(TestOperation.class);
		return suite;
	}
	
	public void main(String[] args) {
		TestRunner.run(suite());
	}
}
